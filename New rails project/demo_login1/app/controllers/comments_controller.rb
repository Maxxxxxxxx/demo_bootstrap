class CommentsController < ApplicationController
	def create
		@article = Article.find(params[:article_id])
		if params[:comment][:body].present?
			@comment = @article.comments.create(comment_params.merge(user_id: current_user.id))
		end
		# @comment.user_id = current_user.id
		# @comment.save
		redirect_to article_path(@article)
	end
	def edit
		@article = Article.find(params[:article_id])
		redirect_to article_path(@article,id_comment_edit: params[:id_comment], flag: true)

	end
	def update
		@article = Article.find(params[:article_id])
		if @article.comments.find(params[:id]).update(comment_params)
			redirect_to article_path(@article)
		else
			redirect_to article_path(@article, flag: true)
		end
	end
	def destroy
		@comment = Comment.find(params[:id_comment])
		@comment.destroy

		redirect_to article_path(Article.all.find(params[:article_id]))
	end
	private
	def comment_params
		params.require(:comment).permit(:body)
	end
end
