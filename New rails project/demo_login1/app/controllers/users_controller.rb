class UsersController < ApplicationController
	skip_before_action :require_login, only: [:new,:create,:find_pass]
	def index
		@users = User.where.not(id: current_user.id).order('created_at asc').paginate(:page => params[:page], :per_page => 5)
	end
	def show
		# if !session[:user_id].present?
		# 	redirect_to login_path
		# end
		@user = User.find(params[:id])
		@articles = @user.articles.order('created_at desc').paginate(:page => params[:page], :per_page => 5)
	end
	def new
		@user = User.new
	end
	def create
		#params[:user][:name].include?(' ')
		# @user = User.new user_params
		# if @user.name.include?(' ')
		# 	flash[:warning] = "Have space in your name"
		# 	render :new
		# elsif User.find_by(name: params[:user][:name]).present?
		# 	flash[:warning] = "Register failed. Your name have existed."
		# 	render :new
		# elsif @user.save
		# 	flash[:success] = "Register success"
		# 	redirect_to users_path
		# else
		# 	flash[:warning] =  "Register failed"
		# 	render :new
		# end
		# if params[:user][:name].include?(' ') || User.find_by(name: params[:user][:name]).present?
		# 	render :new
		# else
		# 	@user = User.new user_params
		# 	if @user.save
				
		# 	end
		# end
		message = params[:user][:name].include?(' ') ? "Have space in your name" : 
					(User.find_by(name: params[:user][:name]).present? ? "Register failed. Your name have existed." 
							: nil)
		@user = User.new user_params
		@user.authorities = false
		unless is_a_valid_email?(params[:user][:email])
			message = "Email invalid"
		end
		if message.present?
			flash[:warning] = message
			render :new
		else
			if @user.save
				flash[:success] = "Register success"
				redirect_to users_path
			else
				flash[:warning] =  "Register failed"
				render :new
			end
		end
	end
	def edit
		@user = User.find(params[:id])
	end
	def update
		user = User.find(params[:id])
		if params[:user][:new_password].present? || params[:user][:confirm_new_password].present?
			if params[:user][:new_password] != params[:user][:confirm_new_password] 
				flash[:warning] = "Two password don't match."
	  			render :edit
			end
		end
	  	if user && user.authenticate(params[:user][:password]) && user.update(user_params)
	   		if params[:user][:new_password].present?
	   			user.password = params[:user][:new_password]
	   			user.password_confirmation = params[:user][:confirm_new_password]
	   		end
	   		if user.save
	   			flash[:success] = "Update successed"
	   			redirect_to users_path
	  		else
	  			flash[:danger] = "Update failed"
	  			render :edit
	  		end
	  	end
	end
	def destroy
		@user = User.find_by id: params[:id]
		if !params[:admin] 
			log_out
			@user.destroy
			redirect_to login_path
		else 
			@user.destroy
			redirect_to users_path(current_user)
		end
		

		
	end
	def find_pass
		user = User.find_by name: params[:user][:name]
		message = !user.present? ? "User name not exist." : 
			( user.email != params[:user][:email] ? "Email Invalid." : 
				( user.update(user_params) && user.save ? "Successed" : "Two password don't match." ))
		# unless user.present?
		# 	flash[:warning] = "User name not exist."
		# 	redirect_to login_path(flag: true)
		# end
		# if user.email != params[:user][:email]
		# 	flash[:warning] = "Email Invalid."
		# 	redirect_to login_path(flag: true)	
		# end
		# if user.update(user_params)
	 #   		if user.save
	 #   			flash[:success] = "Successed"
	 #   			redirect_to login_path
	 #  		else
		# 		flash[:warning] = "Two password don't match."
		# 		redirect_to login_path(flag: true)	
	 #  		end
	 #  	end
	 	if message == "Successed"
	 		flash[:success] = message
	 		redirect_to login_path
	 	else
	 		redirect_to login_path(flag: true,message: message)	
	 	end
	end
	private
	def user_params
		params.require(:user).permit :name, :full_name, :email, :password, :password_confirmation
	end
	
	
end
