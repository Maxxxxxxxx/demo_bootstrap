class User < ApplicationRecord
	has_many :posts, -> {distinct}, dependent: :destroy
end
