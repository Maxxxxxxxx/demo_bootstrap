class ArticlesController < ApplicationController
	def new
		@article = Article.new
	end
	def show
		@article = Article.find(params[:id])
	end
	def index
		@articles = Article.order('created_at desc').paginate(:page => params[:page], :per_page => 7 )
	end
	def create
		@article = Article.new(article_params)

		@article.user_id = current_user.id
		@article.save
		if @article.errors.present?
			flash[:warning] = @article.errors.full_messages.join(", ")
			render :new
		else redirect_to user_path(current_user)
		end
	end
	def edit
		@article = Article.find(params[:id])
		if current_user.id != @article.user_id
  			flash[:danger] = "Invalid path"
			redirect_to articles_path
		end
	end
	def update
	  @article = Article.find(params[:id])
	 
	  if @article.update(article_params)
	    redirect_to @article
	  else
	    render 'edit'
	  end
	end
	def destroy
		@article = Article.find(params[:id])
		if !current_user.authorities
			id_of_user = @article.user_id
			@article.destroy
			redirect_to user_path(User.find(id_of_user))
		else
			@article.destroy
			redirect_to articles_path
		end
	end
	private
	def article_params
		params.require(:article).permit(:title, :text)
	end
end
