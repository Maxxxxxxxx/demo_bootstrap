str = "abcasdf"
puts "#{str[2,str.length]}"
ROMAN={
	"I"=>1,
	"IV"=>4,
	"V"=>5,
	"IX"=>9,
	"X"=>10,
	"XL"=>40,
	"L"=>50,
	"XC"=>90,
	"C"=>100,
	"CD"=>400,
	"D"=>500,
	"CM"=>900,
	"M"=>1000
}
$arr_num = Array[1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1]

$arr_letter = Array["M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"]
$arr_roman2num = $arr_letter.zip($arr_num)

 #array with roman  numeral
def roman2num(roman_n)
	num = 0
	while roman_n!="" do 
		
		$arr_letter.map { |e| if roman_n.start_with? e then 
								roman_n = roman_n[e.length,roman_n.length]
								num = num + ROMAN[e]
							end
		}
	end
	num

end
roman_n = "IIII"

puts "#{roman2num(roman_n)}"
