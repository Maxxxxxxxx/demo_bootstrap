Rails.application.routes.draw do
  devise_for :admins, controllers: {
    sessions: 'admin/sessions'
  }
  # devise_for :admins
  get "login" => "session#new"
  post "login" => "session#create"
  delete "logout" => "session#destroy"
  root "articles#index"
  post "forgot_pass" => "users#find_pass"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :users
  resources :articles do
  	resources :comments
  end
  namespace :admin do
    resources :users  
    resources :articles do
      resources :comments
    end
    root 'admin#show'
  end
end
