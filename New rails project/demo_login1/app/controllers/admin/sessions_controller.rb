class Admin::SessionsController < Devise::SessionsController
	skip_before_action :require_login
	private
	
	def after_sign_out_path_for(resource)
		login_path
	end
end