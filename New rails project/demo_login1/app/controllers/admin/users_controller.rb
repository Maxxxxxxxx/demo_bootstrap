class Admin::UsersController < Admin::BaseController
	def index
		@users = User.order('created_at asc').paginate(:page => params[:page], :per_page => 5)
	end
	def edit
		@user = User.find(params[:id])
	end
	def update
		user = User.find(params[:id])
		if params[:user][:new_password].present? || params[:user][:confirm_new_password].present?
			if params[:user][:new_password] != params[:user][:confirm_new_password] 
				flash[:warning] = "Two password don't match."
	  			render :edit
			end
		end
	  	if user && user.update(user_params)
	   		if params[:user][:new_password].present?
	   			user.password = params[:user][:new_password]
	   			user.password_confirmation = params[:user][:confirm_new_password]
	   		end
	   		if user.save
	   			flash[:success] = "Update successed"
	   			redirect_to admin_users_path
	  		else
	  			flash[:danger] = "Update failed"
	  			render :edit
	  		end
	  	end
	end
	def destroy
		@user = User.find_by id: params[:id]
		@user.destroy
		redirect_to admin_users_path
		
		

		
	end
	private
	def user_params
		params.require(:user).permit :name, :full_name, :email, :password, :password_confirmation
	end
end
