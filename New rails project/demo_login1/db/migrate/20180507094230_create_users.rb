class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :name
      t.string :full_name
      t.string :password_digest
      t.string :email
      t.boolean :authorities

      t.timestamps
    end
  end
end
