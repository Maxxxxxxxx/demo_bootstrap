class SessionController < ApplicationController
  skip_before_action :require_login, only: [:new,:create]
  def new
    if session[:user_id].present?
      redirect_to user_path(session[:user_id])
    end
    @user = User.new
  end
  def create
  	user = User.find_by name: params[:session][:name].downcase
  	if user && user.authenticate(params[:session][:password])
      flash[:success] = "Login success"
      log_in user
      redirect_to articles_path
  	else
  		flash[:danger] = "Invalid email/password combination"
  		redirect_to login_path
  	end
  end
  def destroy
  	log_out
    flash[:success] = "You are logged out"
    redirect_to login_path
  end

end
